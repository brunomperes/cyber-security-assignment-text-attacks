package brunomperes;

import java.util.ArrayList;
import java.util.List;

import FormatIO.Console;
import FormatIO.EofX;
import FormatIO.FileIn;

/**
 * 
 * @author Bruno Peres
 * 
 */
public class KPT {

	final static String name_cipher = "1c.txt";
	final static String name_plain = "1p.txt";
	final static String name_out = "out.txt";

	static int plainText;
	static List<Integer> cipherText;

	public static void main(String[] arg) throws EofX {

		FileIn fcipher;
		FileIn fplain;

		int key;
		boolean foundKey = false;
		StringBuilder finalMessage = new StringBuilder();

		// create a Console for IO
		Console con = new Console("KPT");

		// open files
		fcipher = new FileIn(name_cipher);
		fplain = new FileIn(name_plain);

		plainText = readAndConvertToDec(fplain);
		cipherText = loadText(fcipher);
		
		fcipher.close();
		fplain.close();

		for (key = 0; key < 0xffff; key++) {
			if (plainText == Coder.decrypt(key, cipherText.get(0))) {
				foundKey = true;
				con.println("Key found: " + key);
				break;
			}
		}

		if (foundKey) {
			con.println("Decoded message:");
			for (Integer string : cipherText) {
				String s = block2Text(Coder.decrypt(key, string));
				finalMessage.append(s);
			}
			con.println(finalMessage.toString());
			System.out.println(finalMessage.toString());
		}

		con.println("-- Finished --");
	}

	static List<Integer> loadText(FileIn fcipher) {
		List<Integer> result = new ArrayList<Integer>();
		
		try {
			for (;;) {
				result.add(readAndConvertToDec(fcipher));
			}
		} catch (EofX e) {

		}
		return result;
	}

	static Integer readAndConvertToDec(FileIn f) throws EofX {
		String s = f.readWord();
		return Hex16.convert(s);
	}
	
	/**
	 * Based on the class Block2Text
	 * 
	 * @param value
	 * @return textual value of parameter
	 */
	private static String block2Text(Integer value) {
		String result = "";
		int i = value;
		int c0 = i / 256;
		int c1 = i % 256;
		result += (char) c0;
		if (c1 != 0) {
			result += (char) c1;
		}

		return result;
	}

}
