package brunomperes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import FormatIO.EofX;
import FormatIO.FileIn;

/**
 * 
 * @author Bruno Peres
 * 
 */
public class CTO {

	final static String name_cipher = "2c.txt";
	final static double MAJORITY_INDEX = 0.80;

	static List<String> hexCipherText;
	static List<Integer> intCipherText;
	static List<String> stringCipherText;

	public static class TextBreakAttempt {
		int key = 0;
		List<String> decryptedText = null;
		Integer textSize = 0;
		int numPrintableChars = 0;
		int numLettersAndSpaces = 0;
		Integer numSpaces = 0;

		public TextBreakAttempt() {

		}
	}

	public static void main(String[] arg) {

		FileIn fcipher;

		fcipher = new FileIn(name_cipher);

		hexCipherText = loadText(fcipher);
		fcipher.close();

		Integer key = 0;

		List<TextBreakAttempt> attempts = new ArrayList<TextBreakAttempt>();

		for (key = 0; key < 0xffff; key++) {

			TextBreakAttempt currentAttempt = new TextBreakAttempt();
			currentAttempt.decryptedText = decryptText(hexCipherText, key);
			currentAttempt.key = key;
			
			findTextSize(currentAttempt);
			findNumberOfPrintableChars(currentAttempt);
			if (isMajorityPrintableChars(currentAttempt)) {

				findNumberOfLettersOrSpaces(currentAttempt);
				if (isMajorityLettersOrSpaces(currentAttempt)) {
					attempts.add(currentAttempt);
				}
			}
		}

		printSortedBySpace(attempts);
	}

	private static void findTextSize(TextBreakAttempt attempt) {
		for (String string : attempt.decryptedText) {
			attempt.textSize += string.length();
		}
	}

	private static void printSortedBySpace(List<TextBreakAttempt> attempts) {
		Map<Integer, TextBreakAttempt> orderBySpaceCounting = new TreeMap<>();

		for (TextBreakAttempt possibleSuccessfulAttempt : attempts) {

			findSpaceCount(possibleSuccessfulAttempt);
			orderBySpaceCounting.put(possibleSuccessfulAttempt.numSpaces,
					possibleSuccessfulAttempt);
		}

		for (Integer spaceNums : orderBySpaceCounting.keySet()) {
			System.out.println("spaces: " + spaceNums);
			System.out.println("key: "
					+ orderBySpaceCounting.get(spaceNums).key);
			printText(orderBySpaceCounting.get(spaceNums).decryptedText);
		}

	}

	private static List<String> decryptText(List<String> hexText, int key) {
		List<String> result = new ArrayList<>();

		int intValue; // Integer value for current word
		Integer intDecryptedValue;
		String s;
		for (String hexWord : hexText) {
			intValue = Hex16.convert(hexWord);
			intDecryptedValue = Coder.decrypt(key, intValue);
			s = block2Text(intDecryptedValue);
			result.add(s);
		}
		return result;
	}

	static void findNumberOfPrintableChars(TextBreakAttempt attempt) {
		int size = 0;
		int gibberishChars = 0;
		Character currentChar;

		for (String string : attempt.decryptedText) {
			size += string.length();
			for (int i = 0; i < string.length(); i++) {
				currentChar = string.charAt(i);
				if (currentChar < 32 || currentChar > 126) {
					// According to the ASCII table
					gibberishChars++;
				}
			}
		}
		int printableChars = size - gibberishChars;
		attempt.numPrintableChars = printableChars;
	}

	static boolean isMajorityPrintableChars(TextBreakAttempt attempt) {
		if (((double) attempt.numPrintableChars / attempt.textSize) > MAJORITY_INDEX) {
			return true;
		}
		return false;
	}

	static void findNumberOfLettersOrSpaces(TextBreakAttempt attempt) {
		int letters = 0;
		Character currentChar;

		for (String string : attempt.decryptedText) {
			for (int i = 0; i < string.length(); i++) {
				currentChar = string.charAt(i);
				if (Character.isLowerCase(currentChar)
						|| Character.isUpperCase(currentChar)
						|| Character.isWhitespace(currentChar)) {
					letters++;
				}
			}
		}
		attempt.numLettersAndSpaces = letters;
	}

	static boolean isMajorityLettersOrSpaces(TextBreakAttempt attempt) {
		if (((double) attempt.numLettersAndSpaces / attempt.textSize) > MAJORITY_INDEX) {
			return true;
		}
		return false;
	}

	static void findSpaceCount(TextBreakAttempt attempt) {
		int spaces = 0;
		Character currentChar;

		for (String string : attempt.decryptedText) {
			for (int i = 0; i < string.length(); i++) {
				currentChar = string.charAt(i);
				if (currentChar == ' ') {
					spaces++;
				}
			}
		}
		attempt.numSpaces = spaces;
	}

	static List<String> loadText(FileIn file) {

		List<String> destination = new ArrayList<String>();
		try {
			for (;;) {
				destination.add(file.readWord());
			}
		} catch (EofX e) {

		}
		return destination;
	}

	/**
	 *  Based on the class Block2Text
	 * 
	 * @param value
	 * @return textual value of parameter
	 */
	private static String block2Text(Integer value) {
		String result = "";
		int i = value;
		int c0 = i / 256;
		int c1 = i % 256;
		result += (char) c0;
		if (c1 != 0) {
			result += (char) c1;
		}

		return result;
	}

	static void printText(List<String> text) {
		StringBuilder sb = new StringBuilder();
		for (String string : text) {
			sb.append(string);
		}
		System.out.println(sb.toString());
	}

}
