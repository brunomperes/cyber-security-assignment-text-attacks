package brunomperes;

import java.util.Random;

import FormatIO.EofX;
import FormatIO.FileIn;
import FormatIO.FileOut;

/**
 * @author Bruno Peres
 * 
 */
public class TMT1 {

	final static String name_plain = "3p.txt";
	final static String name_output_table = "3out.txt";
	static final int CHAIN_LENGTH = 12; // L
	private static final int NUMBER_OF_CHAINS = 5000; // N
	private static final int MAXIMUM_KEY_VALUE = 0xffff; // Used for maximum random int

	static int plainText;

	public static void main(String[] arg) throws EofX {
		FileIn fcipher;
		fcipher = new FileIn(name_plain);

		plainText = readAndConvertToDec(fcipher);
		
		fcipher.close();

		Table t = new Table();

		createChains(t);

		printTable(t);

		System.out.println("Table generated");

	}

	static void createChains(Table t) {
		Random randomGenerator = new Random();

		for (int i = 0; i < NUMBER_OF_CHAINS; i++) {

			// Constructs one Chain
			int x0 = randomGenerator.nextInt(MAXIMUM_KEY_VALUE + 1);
			int currentX = x0;
			for (int j = 1; j <= CHAIN_LENGTH; j++) {
				currentX = Coder.encrypt(currentX, plainText);
			}

			// At the end of loop, currentX = equals xL
			// Adds to the table (xL, x0)
			t.add(currentX, x0);
		}
	}

	static void printTable(Table t) {
		FileOut fout = new FileOut(name_output_table);

		for (int xL = 0; xL < 0xffff; xL++) {
			int x0 = t.find(xL);
			if (x0 != -1) {
				String out = String.format("%d %d", xL, x0);
				fout.println(out);
			}
		}
		fout.close();
	}

	static Integer readAndConvertToDec(FileIn f) throws EofX {
		String s = f.readWord();
		return Hex16.convert(s);
	}
}
