package brunomperes;

import java.util.ArrayList;
import java.util.List;

import FormatIO.EofX;
import FormatIO.FileIn;

/**
 * 
 * @author Bruno Peres
 *
 */
public class TMT2 {

	final static String name_cipher = "3c.txt";
	final static String name_plain = "3p.txt";
	final static String name_input_table = "3out.txt";

	static List<Integer> cipherText;
	static List<String> decryptedText;
	static Integer plainText;

	public static void main(String[] arg) throws EofX {
		FileIn fcipher, fplain;
		fcipher = new FileIn(name_cipher);
		fplain = new FileIn(name_plain);

		Table t = new Table();

		loadTable(t);

		plainText = readAndConvertToDec(fplain);
		cipherText = loadText(fcipher);
		
		fplain.close();
		fcipher.close();

		int key = findCipher(t, cipherText.get(0));

		if (key == -1) {
			System.out.println("Key not found");
			return;
		}

		System.out.println("Key found: " + key);

		decryptedText = decryptText(cipherText, key);

		printText(decryptedText);
	}

	private static int findCipher(Table t, Integer cipher) {
		int key = 0;
		int x0 = t.find(cipher);
		if (x0 != -1) {
			// Cipher is an end value
			// Computes the chain again to find xL-1
			int currentX = x0;
			for (int i = 1; i <= TMT1.CHAIN_LENGTH - 1; i++) {
				currentX = Coder.encrypt(currentX, plainText);
			}
			key = currentX;
			return key;
		} else {
			// Cipher is a middle value
			int c1 = Coder.encrypt(cipher, plainText);
			int currentC = c1;
			for (int i = 1; i < TMT1.CHAIN_LENGTH; i++) {
				x0 = t.find(currentC);
				if (x0 != -1) {
					// C was found on table
					// Reconstructs chain
					int currentX = x0;
					for (int j = 1; j < TMT1.CHAIN_LENGTH - i; j++) {
						currentX = Coder.encrypt(currentX, plainText);
					}
					key = currentX;
					return key;
				} else {
					// Keep searching for C on the table
					currentC = Coder.encrypt(currentC, plainText);
				}
			}
		}
		return -1;
	}

	static List<String> decryptText(List<Integer> cipherText, int key) {
		List<String> text = new ArrayList<String>();

		for (Integer cipherWord : cipherText) {
			int intDecryptedValue = Coder.decrypt(key, cipherWord);
			String s = block2Text(intDecryptedValue);
			text.add(s);
		}
		return text;
	}

	private static String block2Text(Integer value) {
		String result = "";
		int i = value;
		int c0 = i / 256;
		int c1 = i % 256;
		result += (char) c0;
		if (c1 != 0) {
			result += (char) c1;
		}

		return result;
	}

	static List<Integer> loadText(FileIn file) {
		List<Integer> destination = new ArrayList<Integer>();
		try {
			for (;;) {
				destination.add(Hex16.convert(file.readWord()));
			}
		} catch (EofX e) {

		}
		return destination;
	}

	static void loadTable(Table t) {
		FileIn ftable;
		ftable = new FileIn(name_input_table);

		try {
			for (;;) {
				int xL = Integer.parseInt(ftable.readWord());
				int x0 = Integer.parseInt(ftable.readWord());
				t.add(xL, x0);
			}
		} catch (EofX e) {

		}
	}

	static void printText(List<String> text) {
		StringBuilder sb = new StringBuilder();
		for (String string : text) {
			sb.append(string);
		}
		System.out.println(sb.toString());
	}

	static Integer readAndConvertToDec(FileIn f) throws EofX {
		String s = f.readWord();
		return Hex16.convert(s);
	}

}
